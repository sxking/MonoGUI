////////////////////////////////////////////////////////////////////////////////
// @file OClock.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#if !defined(__OCLOCK_H__)
#define __OCLOCK_H__


class OClock
{
private:
	OApp* m_pApp;

	// 状态：1: 显示 0:不显示
	int  m_nStatus;
	char m_sTime[128];
	char m_sDate[128];
	LCD* m_pBuffer;

	ULONGLONG m_lLastTime;

	int m_nClockX;
	int m_nClockY;

#if defined (MOUSE_SUPPORT)
	int  m_nClockButtonX; // 时钟按钮的X
	int  m_nClockButtonY; // 时钟按钮的Y
#endif // defined(MOUSE_SUPPORT)

public:
	OClock (OApp* pApp);
	virtual ~OClock();

public:
	void Enable (BOOL bEnable);

	BOOL IsEnable();

	BOOL Check (LCD* pLCD, LCD* pBuf);

#if defined (MOUSE_SUPPORT)
	// 鼠标点击时钟按钮的处理
	BOOL PtProc (int x, int y);

	// 显示时钟按钮
	void ShowClockButton (LCD* pLCD);

	// 设置表盘的显示位置
	BOOL SetClockPos (int x, int y);

	// 设置时钟按钮的位置
	BOOL SetClockButton (int x, int y);

#endif // defined(MOUSE_SUPPORT)
};

#endif // !defined(__OCLOCK_H__)
