////////////////////////////////////////////////////////////////////////////////
// @file BWImgMgt.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

BWImgMgt::BWImgMgt()
{
	m_nCount = 0;
	m_nReserveSize = 0;
	m_pcaImages = NULL;
}

BWImgMgt::~BWImgMgt()
{
	Clear();
}

void BWImgMgt::Clear()
{
	delete [] m_pcaImages;
	m_nCount = 0;
	m_nReserveSize = 0;
	m_pcaImages = NULL;
}

BOOL BWImgMgt::Reserve (int nSize)
{
	if (nSize != m_nReserveSize) {

		BW_IMAGE_ITEM* temp_images = new BW_IMAGE_ITEM[nSize];
		if (temp_images == NULL) {
			return FALSE;
		}

		m_nReserveSize = nSize;

		int copy_size = min(m_nReserveSize, m_nCount);
		if (copy_size > 0) {
			memcpy (temp_images, m_pcaImages,
				sizeof(BW_IMAGE_ITEM) * copy_size);
		}
		if (m_pcaImages != NULL) {
			delete [] m_pcaImages;
		}
		m_pcaImages = temp_images;
		m_nCount = copy_size;
	}
	return TRUE;
}

BOOL BWImgMgt::Add (int nImgID, BW_IMAGE* pImg)
{
	if (m_nCount < m_nReserveSize) {

		m_pcaImages[m_nCount].id = nImgID;
		m_pcaImages[m_nCount].pImage = pImg;
		m_nCount ++;
	}
	else {

		if (! Reserve (m_nReserveSize + 1)) {
			return FALSE;
		}
		m_pcaImages[m_nCount].id = nImgID;
		m_pcaImages[m_nCount].pImage = pImg;
		m_nCount ++;
	}

	return TRUE;
}

BW_IMAGE* BWImgMgt::GetImage (int nImgID)
{
	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (m_pcaImages[i].id == nImgID)
			return m_pcaImages[i].pImage;
	}

	return NULL;
}

/* END */