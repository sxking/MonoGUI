////////////////////////////////////////////////////////////////////////////////
// @file OAccell.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OACCELL_H__)
#define __OACCELL_H__

typedef struct _ACCELL
{
	int    nKeyValue;
	int    id;
	struct _ACCELL* next;
} ACCELL;


class OAccell
{
private:
	int     m_nCount;
	ACCELL* m_pAccell;

public:
	OAccell();
	virtual ~OAccell();

	// 初始化快捷键列表
	BOOL Create (char* sAccellTable, KeyMap* pKeyMap);

	// 搜索快捷键列表
	int Find (int nKeyValue);

private:
	// 删除所有快捷键表项
	BOOL RemoveAll ();
};

#endif // !defined(__OACCELL_H__)
