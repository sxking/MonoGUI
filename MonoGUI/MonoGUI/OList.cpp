////////////////////////////////////////////////////////////////////////////////
// @file OList.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OList::OList()
	: OWindow(self_type)
{
	m_dwAddData1 = 0;		// 用于存放当前有多少调列表项，初始为零
	m_dwAddData2 = 0;		// 用于存放当前显示列表中最上面一条的Index值，初始为零
	m_dwAddData3 = (DWORD)-1;	// 用于存放当前选中的列表项，初始为－1
	m_pContent = NULL;
}

OList::~OList()
{
	RemoveAll();
}

// 创建列表框
BOOL OList::Create
(
	OWindow* pParent,			// 父窗口指针
	WORD wStyle,				// 窗口的样式
	WORD wStatus,				// 窗口的状态
	int x,
	int y,
	int w,
	int h,						// 绝对位置
	int ID						// 窗口的ID号
)
{
	if (!OWindow::Create(pParent, wStyle, WND_STATUS_NORMAL, x, y, w, h, ID)) {
		DebugPrintf("ERROR: OList::Create OWindow::Create fail!\n");
		return FALSE;
	}

	m_dwAddData4 = (m_h - 3) / LIST_ITEM_H;		// 一页能够显示的条目数
	RenewScroll();
	return TRUE;
}

// 绘制列表框
void OList::Paint(LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible()) {
		return;
	}

	int crBk = 1;
	int crFr = 0;

	// 绘制边框，填充背景
	pLCD->FillRect (m_x, m_y, m_w, m_h, crBk);
	if ((m_wStyle & WND_STYLE_NO_BORDER) == 0)
	{
		pLCD->HLine (m_x, m_y, m_w, crFr);
		pLCD->HLine (m_x, m_y+m_h-1, m_w, crFr);
		pLCD->VLine (m_x, m_y, m_h, crFr);
		pLCD->VLine (m_x+m_w-1, m_y, m_h, crFr);
	}

	// 如果指定了立体效果，则绘制阴影
	if ((m_wStyle & WND_STYLE_SOLID) > 0)
	{
		pLCD->HLine (m_x+1, m_y+m_h, m_w, 1);
		pLCD->VLine (m_x+m_w, m_y+1, m_h, 1);
	}

	// 绘制列表内容
	int nCount			= m_dwAddData1;
	int nTopItem		= m_dwAddData2;
	int nCurSel			= m_dwAddData3;
	int nCanDiaplayed	= m_dwAddData4;

	char sTemp[LIST_TEXT_MAX_LENGTH];
	int i;
	for (i = 0; i < nCanDiaplayed; i++)
	{
		if (nTopItem + i >= nCount )
			break;

		// 取得要显示的字符串
		GetString (nTopItem + i, sTemp);
		int nTextLength = GetStringLength (nTopItem + i);
		// 计算可以显示的长度
		int nWidth = m_w - 4;
		if ((m_pVScroll != NULL) && ((m_wStyle & WND_STYLE_NO_SCROLL) == 0))
		{
			if (m_pVScroll->m_nStatus == 1)              // 滚动条处于显示状态
				nWidth = nWidth - SCROLL_WIDTH + 1;      // 减去滚动条的宽度
		}

		int nLengthLimit = GetDisplayLimit_Align (sTemp, nWidth);

		if (nTopItem + i != nCurSel)
		{
			pLCD->TextOut_Align (m_x+2, m_y+LIST_ITEM_H*i+2, (BYTE*)sTemp, nLengthLimit, LCD_MODE_NORMAL);
		}
		else
		{
			// 如果本控件处于焦点，则反白显示选中条目
			int crBk = 0;
			int crSelect = 2;

			// 如果不是焦点，则用虚线框圈起选中条目
			if ((m_wStatus & WND_STATUS_FOCUSED) > 0)
			{
				pLCD->FillRect (m_x+2, m_y+LIST_ITEM_H*i+2, nWidth, HZK_H, crBk);
				pLCD->TextOut_Align (m_x+2, m_y+LIST_ITEM_H*i+2, (BYTE*)sTemp, nLengthLimit, LCD_MODE_INVERSE);
			}
			else
			{
				pLCD->TextOut_Align (m_x+2, m_y+LIST_ITEM_H*i+2, (BYTE*)sTemp, nLengthLimit, LCD_MODE_NORMAL);
				pLCD->FillRect (m_x+2,m_y+LIST_ITEM_H*i+1,nWidth+1,1,crSelect);            // 上
				pLCD->FillRect (m_x+1,m_y+LIST_ITEM_H*i+1,1,HZK_H+2,crSelect);             // 左
				pLCD->FillRect (m_x+2,m_y+LIST_ITEM_H*(i+1)+1,nWidth+1,1,crSelect);        // 下
				pLCD->FillRect (m_x+nWidth+2,m_y+LIST_ITEM_H*i+1,1,HZK_H+2,crSelect);      // 右
			}
		}
	}

	// 先调用基类的绘制函数(用于绘制滚动条)
	OWindow::Paint(pLCD);
}

// 消息处理
int OList::Proc (OWindow *pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nCount			= m_dwAddData1;
	int nTopItem		= m_dwAddData2;
	int nCurSel			= m_dwAddData3;
	int nCanDisplayed	= m_dwAddData4;

	int nReturn = 0;

	if (nMsg == OM_KEYDOWN)
	{
		switch (wParam)
		{
		case KEY_UP:
			{
				// 向上移动选项
				nCurSel -= 1;
				if (nCurSel < 0)
					nCurSel = 0;

				SetCurSel (nCurSel);

				if (nTopItem > nCurSel)
					nTopItem = nCurSel;
				if (nTopItem + nCanDisplayed-1 < nCurSel)
					nTopItem = nCurSel - nCanDisplayed + 1;

				m_dwAddData2 = nTopItem;

				// 更新滚动条
				RenewScroll();
				nReturn = 1;
			}
			break;

		case KEY_DOWN:
			{
				// 向下移动选项
				nCurSel += 1;
				if (nCurSel > nCount-1)
					nCurSel = nCount-1;
				if (nCurSel < 0)
					nCurSel = 0;

				SetCurSel (nCurSel);

				if (nTopItem > nCurSel)
					nTopItem = nCurSel;
				if (nTopItem + nCanDisplayed - 1 < nCurSel)
					nTopItem = nCurSel - nCanDisplayed + 1;

				m_dwAddData2 = nTopItem;

				// 更新滚动条
				RenewScroll();
				nReturn = 1;
			}
			break;

		case KEY_PAGE_UP:
			{
				// 向前翻页
				nCurSel -= nCanDisplayed;
				if (nCurSel < 0)
					nCurSel = 0;

				SetCurSel (nCurSel);

				if (nTopItem > nCurSel)
					nTopItem = nCurSel;
				if (nTopItem + nCanDisplayed - 1 < nCurSel)
					nTopItem = nCurSel - nCanDisplayed + 1;

				m_dwAddData2 = nTopItem;

				// 更新滚动条
				RenewScroll();
				nReturn = 1;
			}
			break;

		case KEY_PAGE_DOWN:
			{
				// 向后翻页
				// 向下移动选项
				nCurSel += nCanDisplayed;

				if (nCurSel > nCount - 1)
					nCurSel = nCount - 1;
				if (nCurSel < 0 )
					nCurSel = 0;

				SetCurSel (nCurSel);

				if (nTopItem > nCurSel)
					nTopItem = nCurSel;
				if (nTopItem + nCanDisplayed - 1 < nCurSel)
					nTopItem = nCurSel - nCanDisplayed + 1;

				m_dwAddData2 = nTopItem;

				// 更新滚动条
				RenewScroll();
				nReturn = 1;
			}
			break;

		case KEY_ENTER:			// 传送给输入法的确认键
			{
				if (IsWindowEnabled() &&
				    ((m_wStyle & WND_STYLE_IGNORE_ENTER) == 0))
				{
					O_MSG msg;
					msg.pWnd    = m_pParent;
					msg.message = OM_KEYDOWN;
					msg.wParam  = KEY_TAB;
					msg.lParam  = 0;
					m_pApp->PostMsg (&msg);
					nReturn = 1;
				}
				else {
					nReturn = 0;
				}
			}
			break;
		}
	}
	else if (nMsg == OM_SETFOCUS)
	{
		// 得到焦点，如果没有处于处于选择状态的条目应当将第一条设置为当前选项
		// 并且移动显示列表将当前选项放置在合适的位置
		if (pWnd == this)
		{
			if (nCurSel == -1)
				nCurSel = 0;

			if (nTopItem > nCurSel)
				nTopItem = nCurSel;
			if (nTopItem + nCanDisplayed - 1 < nCurSel)
				nTopItem = nCurSel - nCanDisplayed + 1;

			m_dwAddData2 = nTopItem;
			m_dwAddData3 = nCurSel;
			
			RenewScroll();
		}
	}

	return nReturn;
}

#if defined (MOUSE_SUPPORT)
// 坐标设备消息处理
int OList::PtProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = OWindow::PtProc (pWnd, nMsg, wParam, lParam);

	if (nReturn != 1)
	{
		if (nMsg == OM_LBUTTONDOWN)
		{
			if (PtInWindow (wParam, lParam))
			{
				if (IsWindowEnabled())
				{
					int x = wParam;
					int y = lParam;
					int nCurSel = PtInItems (x, y);
					if (nCurSel != -1)
					{
						SetCurSel (nCurSel);
						RenewScroll();
					}
					nReturn = 1;
				}
			}
		}
	}

	return nReturn;
}

// 测试坐标落于的条目，-1表示未落于任何条目
int OList::PtInItems (int x, int y)
{
	CHECK_TYPE_RETURN;

	int nCount        = m_dwAddData1;
	int nTopItem      = m_dwAddData2;
	int nCurSel       = m_dwAddData3;
	int nCanDiaplayed = m_dwAddData4;

	int nRight = m_x + m_w;
	if ((m_pVScroll != NULL) && ((m_wStyle & WND_STYLE_NO_SCROLL) == 0))
	{
		if (m_pVScroll->m_nStatus == 1)    // 滚动条处于显示状态
			nRight -= SCROLL_WIDTH;        // 减去滚动条的宽度
	}

	int L = m_x+2;
	int T = m_y+1;
	int R = nRight-2;
	int B = T+LIST_ITEM_H*nCanDiaplayed;

	if (! PtInRect (x,y,L,T,R,B))
		return -1;

	B = T+LIST_ITEM_H;

	int i;
	for (i = 0; i < nCanDiaplayed; i++)
	{
		if (nTopItem + i >= nCount)
			return -1;

		if (PtInRect (x,y,L,T,R,B))
			return (nTopItem + i);

		T += LIST_ITEM_H;
		B += LIST_ITEM_H;
	}

	return -1;
}
#endif // defined(MOUSE_SUPPORT)

// 获得条目的数量
int OList::GetCount()
{
	CHECK_TYPE_RETURN;

	return m_dwAddData1;
}

// 获得当前显示区域最上面一个条目的Index
int OList::GetTopIndex()
{
	CHECK_TYPE_RETURN;

	return m_dwAddData2;
}

// 设置当前显示区域最上面一个条目的Index
int OList::SetTopIndex(int nIndex)
{
	CHECK_TYPE_RETURN;

    int nCount		= m_dwAddData1;
	int nTopItem	= m_dwAddData2;

	// 进行数据合法性检查
	if ((nIndex < 0) || (nIndex > nCount - 1)) {
		return -1;
	}

	int nOldTopIndex = nTopItem;
	// 设置列表框中最上面一项索引值
	m_dwAddData2 = nIndex;

	// 刷新屏幕
	UpdateView (this);

	return nOldTopIndex;
}

// 得到当前选中项目的Index，如果没有选中的则返回-1
int OList::GetCurSel()
{
	CHECK_TYPE_RETURN;

	return m_dwAddData3;
}

// 设置当前的选中项目
int OList::SetCurSel (int nIndex)
{
	CHECK_TYPE_RETURN;

	int nCount		= m_dwAddData1;
	int nCurSel		= m_dwAddData3;

	if ((nIndex < 0) || (nIndex > nCount - 1)) {
		return -1;
	}

	// 移动焦点到index指定行的内容。。。
	int nOldIndex = nCurSel;
	m_dwAddData3 = nIndex;

	// 给父窗口发送OM_DATACHANGE消息
	O_MSG Msg;
	Msg.pWnd = m_pParent;
	Msg.message = OM_DATACHANGE;
	Msg.wParam = m_ID;
	Msg.lParam = 0;
	m_pApp->PostMsg(&Msg);

	return nOldIndex;
}

// 获得某一列表项的内容
BOOL OList::GetString (int nIndex, char* pText)
{
	CHECK_TYPE_RETURN;

	int nCount = m_dwAddData1;

	if ((nIndex < 0) || (nIndex > nCount - 1)) {
		return FALSE;
	}

	LISTCONTENT* pNextItem = m_pContent;
	int i;
	for (i = 0; i < nIndex; i++)
	{
		if (pNextItem == NULL)
			return FALSE;
		else
			pNextItem = pNextItem->next;
	}

	// 拷贝iIndex所指定列表项的内容
	if (pNextItem != NULL)
	{
		strncpy (pText, pNextItem->text, LIST_TEXT_MAX_LENGTH-1);
		return TRUE;
	}
	
	return FALSE;
}

// 设置某一列表项的内容
BOOL OList::SetString (int nIndex, char* pText)
{
	int nCount = m_dwAddData1;

	if ((nIndex < 0) || (nIndex > nCount - 1)) {
		return FALSE;
	}

	LISTCONTENT* pNextItem = m_pContent;
	int i;
	for (i = 0; i < nIndex; i++)
	{
		if (pNextItem == NULL)
			return FALSE;
		else
			pNextItem = pNextItem->next;
	}

	// 设置iIndex所指定列表项的内容
	if (pNextItem != NULL)
	{
		memset (pNextItem->text, 0x0, LIST_TEXT_MAX_LENGTH);
		strncpy (pNextItem->text, pText, LIST_TEXT_MAX_LENGTH-1);
		return TRUE;
	}
	
	return FALSE;
}

// 获得某一列表项内容的长度
int OList::GetStringLength (int nIndex)
{
	CHECK_TYPE_RETURN;

	int nCount = m_dwAddData1;

	if ((nIndex < 0) || (nIndex > nCount - 1)) {
		return FALSE;
	}

	LISTCONTENT* pNextItem = m_pContent;
	int i;
	for (i = 0; i < nIndex; i++)
	{
		if (pNextItem == NULL)
			return FALSE;
		else
			pNextItem = pNextItem->next;
	}

	// 测定iIndex所指定列表项内容的长度
	int nLength = 0;
	if (pNextItem != NULL)
		nLength = strlen (pNextItem->text);

	return nLength;
}

// 向列表框中添加一个串(加在末尾)
BOOL OList::AddString (char* pText)
{
	CHECK_TYPE_RETURN;

	int nCount = m_dwAddData1;

	// 创建新的节点
	LISTCONTENT* theNewOne = new LISTCONTENT;
	strncpy (theNewOne->text, pText, LIST_TEXT_MAX_LENGTH-1);
	theNewOne->next = NULL;

	if (nCount == 0)
	{
		m_pContent = theNewOne;
		m_dwAddData1 ++;
		return TRUE;
	}

	// 查找最末一条列表项
	LISTCONTENT* theNext = m_pContent;
	int i;
	for (i = 0; i < nCount - 1; i++)
	{
		if (theNext->next == NULL)
			return FALSE;

		theNext = theNext->next;
	}

	// 最后一条的next指针必须为NULL，否则说明其他程序存在错误
	if (theNext->next != NULL) {
		return FALSE;
	}

	theNext->next = theNewOne;
	m_dwAddData1 ++;

	// 更新滚动条
	RenewScroll();
	return TRUE;
}

// 删除一个列表项
BOOL OList::DeleteString (int nIndex)
{
	CHECK_TYPE_RETURN;

	int nCount  = m_dwAddData1;
	int nCurSel = m_dwAddData3;

	if ((nIndex < 0) || (nIndex > nCount - 1)) {
		return FALSE;
	}

	if (nCount == 1)
	{
		delete m_pContent;
		m_pContent = NULL;
		nCount --;
		if (nCurSel == nIndex)
			nCurSel = -1;

		// 更新
		m_dwAddData1 = nCount;
		m_dwAddData3 = nCurSel;
		RenewScroll();
		return TRUE;
	}

	if (nIndex == 0)
	{
		LISTCONTENT* pNextItem = m_pContent->next;
		delete m_pContent;
		m_pContent = pNextItem;
		nCount --;
		if (nCount == 0)
			nCurSel = -1;

		// 更新
		m_dwAddData1 = nCount;
		m_dwAddData3 = nCurSel;		
		RenewScroll();
		return TRUE;
	}

	LISTCONTENT* pNextItem = m_pContent;
	LISTCONTENT* pLastItem = m_pContent;	// 用于存放要删除节点的上一个节点

	int i;
	for (i = 0; i < nIndex; i++)
	{
		if (pNextItem == NULL)
			return FALSE;
		else
		{
			pLastItem = pNextItem;
			pNextItem = pNextItem->next;
		}
	}

	// 删除iIndex所指定的列表项
	if (pNextItem != NULL)
	{
		pLastItem->next = pNextItem->next;
		delete pNextItem;
		nCount --;
		if (nCurSel >= nCount)
			nCurSel = (nCount -1);

		// 更新
		int nTopItem      = m_dwAddData2;
		int nCanDisplayed = m_dwAddData4;

		if (nTopItem + nCanDisplayed > nCount)
		    nTopItem = nCount - nCanDisplayed;
		if (nTopItem < 0 )
		    nTopItem = 0;

		m_dwAddData1 = nCount;
		m_dwAddData2 = nTopItem;
		m_dwAddData3 = nCurSel;
		RenewScroll();
		return TRUE;
	}

	return FALSE;
}

// 在指定位置插入一个串
BOOL OList::InsertString (int nIndex, char* pText)
{
	CHECK_TYPE_RETURN;

	int nCount		= m_dwAddData1;
	int nCurSel		= m_dwAddData3;

	if (nIndex < 0)
		return FALSE;

	if ((nIndex > 0) && (nIndex > nCount - 1)) {
		nIndex = nCount - 1;
	}

	// 创建新的节点
	LISTCONTENT* theNewOne = new LISTCONTENT;
	strncpy (theNewOne->text, pText, LIST_TEXT_MAX_LENGTH-1);
	theNewOne->next = NULL;

	if (nCount == 0)
	{
		m_pContent = theNewOne;
		m_dwAddData1 ++;

		// 更新滚动条
		RenewScroll();
		return TRUE;
	}

	if (nIndex == 0)
	{
		theNewOne->next = m_pContent;
		m_pContent = theNewOne;
		m_dwAddData1 ++;
		if (nCurSel == 0)
			m_dwAddData3 ++;

		// 更新滚动条
		RenewScroll();
		return TRUE;
	}

	LISTCONTENT* pNextItem = m_pContent;
	LISTCONTENT* pLastItem = m_pContent;	// 用于存放要插入节点的上一个节点

	int i;
	for (i = 0; i < nIndex; i++)
	{
		if (pNextItem == NULL)
			return FALSE;
		else
		{
			pLastItem = pNextItem;
			pNextItem = pNextItem->next;
		}
	}

	// 在nIndex位置插入新的列表项
	if (pNextItem != NULL)
	{
		pLastItem->next = theNewOne;
		theNewOne->next = pNextItem;
		m_dwAddData1 ++;
		if (nCurSel == nIndex)
			m_dwAddData3 ++;

		// 更新滚动条
		RenewScroll();
		return TRUE;
	}
	
	return FALSE;
}

// 删除所有列表项
BOOL OList::RemoveAll()
{
	CHECK_TYPE_RETURN;

	int nCount	= m_dwAddData1;

	LISTCONTENT* theNext = NULL;
	int i;
	for (i = 0; i < nCount; i++)
	{
		if (m_pContent == NULL)
		{
			m_dwAddData1 = 0;
			m_dwAddData2 = 0;
			m_dwAddData3 = (DWORD)-1;
			// 更新滚动条
			RenewScroll();
			return FALSE;
		}

		theNext = m_pContent->next;
		if (m_pContent != NULL )
		{
			delete m_pContent;
		}
		m_pContent = theNext;
	}

	m_dwAddData1 = 0;
	m_dwAddData2 = 0;
	m_dwAddData3 = (DWORD)-1;
	// 更新滚动条
	RenewScroll();
	return TRUE;
}

// 在列表项中查找一个串
int OList::FindString (char* pText)
{
	CHECK_TYPE_RETURN;

	int nCount = m_dwAddData1;

	// 循环查找所有列表项
	LISTCONTENT* theNext = m_pContent;
	int i;
	for (i = 0; i < nCount; i++)
	{
		if (strcmp(theNext->text, pText) == 0) {
			return i;
		}

		if (theNext->next == NULL) {
			return -1;
		}

		theNext = theNext->next;
	}
	return -1;
}

// 在列表项中查找一个串，如果找到，则将它设置为选中，并显示在第一行
// (如果在最后一页，则令最后一行显示在列表框的最底端)
int OList::SelectString (char* pText)
{
	CHECK_TYPE_RETURN;

	int nCount			= m_dwAddData1;
	int nTopItem		= m_dwAddData2;
	int nCanDisplayed	= m_dwAddData4;

	// 从链表中查找指定的字符串
	int nIndex = FindString (pText);
	if (nIndex == -1) {	// 如果没有找到
		return -1;
	}

	SetCurSel (nIndex);

	int nCurSel = m_dwAddData3;
	
	if (nTopItem > nCurSel) {
		nTopItem = nCurSel;
	}

	if (nTopItem + nCanDisplayed - 1 < nCurSel) {
		nTopItem = nCurSel - nCanDisplayed + 1;
	}

	m_dwAddData2 = nTopItem;

	return nIndex;
}

// 调整列表框的高度为整行
BOOL OList::SetLinage (int nLinage)
{
	CHECK_TYPE_RETURN;

	if (nLinage < 1) {
		return FALSE;
	}

	m_dwAddData4 = nLinage;
	m_h = nLinage * (HZK_H + 1) + 3;
	RenewScroll();
	return TRUE;
}

// 更新滚动条
void OList::RenewScroll()
{
	CHECK_TYPE;

	if ((m_wStyle & WND_STYLE_NO_SCROLL) > 0) {
		DebugPrintf ("Debug: OList::RenewScroll Style No Scroll.\n");
		return;
	}
		
	int nCount			= m_dwAddData1;
	int nTopItem		= m_dwAddData2;
	int nCanDisplayed	= m_dwAddData4;

	if (nCount > nCanDisplayed)
	{
		// 显示滚动条
		SetScrollBar (1, nCount, nCanDisplayed, nTopItem);
		ShowScrollBar(1, TRUE);
	}
	else
	{
		// 隐藏滚动条
		ShowScrollBar (1, FALSE);
	}
	UpdateView (this);
}

#if defined (MOUSE_SUPPORT)
// 向上
void OList::OnScrollUp ()
{
	CHECK_TYPE;

	int nCount			= m_dwAddData1;
	int nTopItem		= m_dwAddData2;
	int nCurSel			= m_dwAddData3;
	int nCanDisplayed	= m_dwAddData4;

	if (nTopItem > 0) {
		nTopItem --;
	}

	m_dwAddData2 = nTopItem;
	// 更新滚动条
	RenewScroll ();
}

// 向下
void OList::OnScrollDown ()
{
	CHECK_TYPE;

	int nCount			= m_dwAddData1;
	int nTopItem		= m_dwAddData2;
	int nCurSel			= m_dwAddData3;
	int nCanDisplayed	= m_dwAddData4;

	if (nTopItem + nCanDisplayed < nCount) {
		nTopItem ++;
	}

	m_dwAddData2 = nTopItem;
	// 更新滚动条
	RenewScroll();
}

// 向上翻页
void OList::OnScrollPageUp ()
{
	CHECK_TYPE;

	int nCount			= m_dwAddData1;
	int nTopItem		= m_dwAddData2;
	int nCurSel			= m_dwAddData3;
	int nCanDisplayed	= m_dwAddData4;

	nTopItem -= nCanDisplayed;
	if (nTopItem < 0) {
		nTopItem = 0;
	}

	m_dwAddData2 = nTopItem;
	// 更新滚动条
	RenewScroll();
}

// 向下翻页
void OList::OnScrollPageDown ()
{
	CHECK_TYPE;

	int nCount			= m_dwAddData1;
	int nTopItem		= m_dwAddData2;
	int nCurSel			= m_dwAddData3;
	int nCanDisplayed	= m_dwAddData4;

	nTopItem += nCanDisplayed;
	if (nTopItem + nCanDisplayed >= nCount) {
		nTopItem = nCount - nCanDisplayed;
	}

	m_dwAddData2 = nTopItem;
	// 更新滚动条
	RenewScroll();
}

// 滚动条拖拽
void OList::OnVScrollNewPos (int nNewPos)
{
	CHECK_TYPE;

	// 修改TopItem的值
	m_dwAddData2 = nNewPos;

	// 更新滚动条
	RenewScroll();
}
#endif // defined(MOUSE_SUPPORT)

/* END */
