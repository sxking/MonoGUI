////////////////////////////////////////////////////////////////////////////////
// @file OStatic.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OSTATIC_H__)
#define __OSTATIC_H__

class OStatic : public OWindow
{
private:
	enum { self_type = WND_TYPE_STATIC };

public:
	OStatic ();
	virtual ~OStatic ();

	// 定义静态图像所需要的图片
	BW_IMAGE* m_pImage;

public:
	// 创建编辑框
	BOOL Create
	(
		OWindow* pParent,
		WORD wStyle,
		WORD wStatus,
		int x,
		int y,
		int w,
		int h,
		int ID
	);

	// 调入图片
	BOOL SetImage (BW_IMAGE* pImage);

	void Paint (LCD* pLCD);
	int Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);
};

#endif // !defined(__OSTATIC_H__)
