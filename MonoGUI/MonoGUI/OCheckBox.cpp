////////////////////////////////////////////////////////////////////////////////
// @file OCheckBox.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OCheckBox::OCheckBox ()
	: OWindow(self_type)
{
	m_nCheckState = 0;
}

OCheckBox::~OCheckBox ()
{
}

BOOL OCheckBox::Create (OWindow* pParent,WORD wStyle,WORD wStatus,int x,int y,int w,int h,int ID)
{
	if (!OWindow::Create(pParent, wStyle, wStatus, x, y, w, h, ID)) {
		return FALSE;
	}

	return TRUE;
}

// 虚函数，绘制按钮
void OCheckBox::Paint (LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible())
		return;

	// 计算文字的居中显示位置
	int ty = m_y + (m_h - 12) / 2;

	// 填充背景
	int crBk = 1;
	pLCD->FillRect (m_x, m_y, m_w, m_h, crBk);

	// 根据选择状态绘制选择块
	if (m_nCheckState == 0) {
		pLCD->DrawImage (m_x+3,ty-1,13,13,g_4color_Check_Box_Unselected,0,0, LCD_MODE_NORMAL);
	}
	else if (m_nCheckState == 1) {
		pLCD->DrawImage (m_x+3,ty-1,13,13,g_4color_Check_Box_Selected,0,0, LCD_MODE_NORMAL);
	}

	// 计算文字的限定长度并绘制文字
	int nTextLength = GetDisplayLimit (m_sCaption, m_w-17);
	pLCD->TextOut (m_x+17,ty,(BYTE*)m_sCaption,nTextLength, LCD_MODE_NORMAL);

	// 焦点状态绘制虚线的外框
	if ((m_wStatus & WND_STATUS_FOCUSED) > 0)
	{
		int crFr = 2;
		pLCD->FillRect (m_x,m_y,m_w,1,crFr);
		pLCD->FillRect (m_x,m_y,1,m_h,crFr);
		pLCD->FillRect (m_x,m_y+m_h,m_w,1,crFr);
		pLCD->FillRect (m_x+m_w,m_y,1,m_h,crFr);
	}
}

// 虚函数，消息处理
// 消息处理过了，返回1，未处理返回0
int OCheckBox::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = 0;

	// 只处理三种消息：1、空格键；2、回车键；3、OM_PUSHDOWN消息
	if (nMsg == OM_KEYDOWN)
	{
		if ((wParam == KEY_SPACE) || (wParam == KEY_ENTER))
		{
			if ((m_wStatus & WND_STATUS_FOCUSED) > 0)
			{
				OnCheck ();
			}
			nReturn = 1;
		}
	}
	else if (nMsg == OM_PUSHDOWN)
	{
		if (pWnd == this)
		{
			OnCheck ();
		}
		nReturn = 1;
	}

	return nReturn;
}

#if defined (MOUSE_SUPPORT)
// 坐标设备消息处理
int OCheckBox::PtProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = OWindow::PtProc (pWnd, nMsg, wParam, lParam);

	if (nMsg == OM_LBUTTONDOWN)
	{
		if (PtInWindow (wParam, lParam))
		{
			nReturn = 1;

			// 计算选择方块的位置
			int left   = m_x  + 3;
			int top    = m_y  + (m_h - 12) / 2 - 1;
			int right  = left + 13;
			int bottom = top  + 13;
			if (PtInRect (wParam, lParam, left, top, right, bottom))
				OnCheck();
		}
	}

	return nReturn;
}
#endif // defined(MOUSE_SUPPORT)

// 设置选择状态
BOOL OCheckBox::SetCheck (int nCheck)
{
	CHECK_TYPE_RETURN;

	if ((m_wStatus & WND_STATUS_INVISIBLE) != 0) {
		return FALSE;
	}

	if ((nCheck < 0) || (nCheck > 1)) {
		return FALSE;
	}

	m_nCheckState = nCheck;
	return TRUE;
}

// 得到选择状态
int OCheckBox::GetCheck()
{
	CHECK_TYPE_RETURN;

	return m_nCheckState;
}

// 按键被按下的处理
void OCheckBox::OnCheck ()
{
	CHECK_TYPE;

	DebugPrintf ("OnCheck \n");

	if ((m_wStatus & WND_STATUS_INVISIBLE) != 0)
		return;

	// 刷新显示，向父窗口发出通知消息，再次刷新显示
	if (m_nCheckState == 1)
		m_nCheckState = 0;
	else
		m_nCheckState = 1;

	Paint (m_pApp->m_pLCD);

	// 向父窗口发送消息值为本ID的消息
	O_MSG msg;
	msg.pWnd    = m_pParent;
	msg.message = OM_NOTIFY_PARENT;
	msg.wParam  = m_ID;
	msg.lParam  = m_nCheckState;
	m_pApp->PostMsg (&msg);

	m_pApp->Show();
}

/* END */