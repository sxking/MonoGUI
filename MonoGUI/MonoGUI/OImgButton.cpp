////////////////////////////////////////////////////////////////////////////////
// @file OImgButton.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OImgButton::OImgButton ()
	: OWindow(self_type)
{
	m_pImage = NULL;
}

OImgButton::~OImgButton ()
{
	m_pImage = NULL;
}

BOOL OImgButton::Create (OWindow* pParent,WORD wStyle,WORD wStatus,int x,int y,int w,int h,int ID)
{
	if (!OWindow::Create(pParent, wStyle, wStatus, x, y, w, h, ID)) {
		return FALSE;
	}

	return TRUE;
}

// 调入图片
BOOL OImgButton::SetImage (BW_IMAGE* pImage)
{
	CHECK_TYPE_RETURN;

	if (pImage == NULL) {
		return FALSE;
	}

	m_pImage = pImage;
	return TRUE;
}

// 虚函数，绘制按钮
void OImgButton::Paint (LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible()) {
		return;
	}

	// 计算文字的居中显示位置
	int tx = m_x + (m_w - 6 * GetTextLength()) / 2;
	int ty = m_y + (m_h - 12) / 2;

	// 根据按钮不同的状态绘制
	// 图片从上到下分别是：
	// 1>Normal样式的图片
	// 2>Focus样式的图片
	// 3>PushDown样式的图片

	// 图片宽度按字节对齐
	int nImageW = ((m_w + 7) / 8) * 8;

	// 绘制按下状态的按钮
	if ((m_wStatus & WND_STATUS_PUSHED) > 0)
	{
		// 绘制底图
		pLCD->DrawImage (m_x,m_y,m_w,m_h,
						 *m_pImage,0,m_h*2,LCD_MODE_NORMAL);
		// 绘制反白的文字
		pLCD->TextOut (tx,ty,(BYTE*)m_sCaption,WINDOW_CAPTION_BUFFER_LEN-1, LCD_MODE_INVERSE);
		return;
	}

	// 绘焦点状态的按钮
	if ((m_wStatus & WND_STATUS_FOCUSED) > 0)
	{
		// 绘制底图
		pLCD->DrawImage (m_x,m_y,m_w,m_h,
						 *m_pImage,0,m_h,LCD_MODE_NORMAL);
		// 绘制正常的文字
		pLCD->TextOut (tx,ty,(BYTE*)m_sCaption,WINDOW_CAPTION_BUFFER_LEN-1, LCD_MODE_NORMAL);
		return;
	}

	// 绘制普通样式的按钮
	// 绘制底图
	pLCD->DrawImage (m_x,m_y,m_w,m_h,
					 *m_pImage,0,0,LCD_MODE_NORMAL);
	// 绘制正常的文字
	pLCD->TextOut (tx,ty,(BYTE*)m_sCaption,WINDOW_CAPTION_BUFFER_LEN-1, LCD_MODE_NORMAL);
}

// 虚函数，消息处理
// 消息处理过了，返回1，未处理返回0
int OImgButton::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = 0;

	// 按钮只处理三种消息：1、空格键；2、回车键(只对处于默认状态的按钮有效)；3、OM_PUSHDOWN消息
	if (nMsg == OM_KEYDOWN)
	{
		if (wParam == KEY_SPACE)
		{
			if ((m_wStatus & WND_STATUS_FOCUSED) > 0)
			{
				OnPushDown ();
			}
			nReturn = 1;
		}
		else if (wParam == KEY_ENTER)
		{
			if ((m_wStatus & WND_STATUS_DEFAULT) > 0)
			{
				OnPushDown ();
			}
			nReturn = 1;
		}
	}
	else if (nMsg == OM_PUSHDOWN)
	{
		if (pWnd == this)
		{
			OnPushDown ();
		}
		nReturn = 1;
	}

	return nReturn;
}

#if defined (MOUSE_SUPPORT)
// 坐标设备消息处理
int OImgButton::PtProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = OWindow::PtProc (pWnd, nMsg, wParam, lParam);

	if (nMsg == OM_LBUTTONDOWN)
	{
		if (PtInWindow (wParam, lParam) )
		{
			OnPushDown();
			nReturn = 1;
		}
	}

	return nReturn;
}
#endif // defined(MOUSE_SUPPORT)

// 按键被按下的处理
void OImgButton::OnPushDown ()
{
	CHECK_TYPE;

	DebugPrintf ("OnPushDown \n");
	// 刷新显示，向父窗口发出通知消息，再次刷新显示

	m_wStatus |= WND_STATUS_PUSHED;
	Paint (m_pApp->m_pLCD);

	m_pApp->Show();

	// 延时，使按钮按下的状态可以被看清楚
	Delay (200);

	// 向父窗口发送消息值为本ID的消息
	O_MSG msg;
	msg.pWnd    = m_pParent;
	msg.message = OM_NOTIFY_PARENT;
	msg.wParam  = m_ID;
	msg.lParam  = 0;
	m_pApp->PostMsg (&msg);

	m_wStatus &= ~WND_STATUS_PUSHED;
	Paint(m_pApp->m_pLCD);

	m_pApp->Show();
}

/* END */