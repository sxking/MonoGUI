////////////////////////////////////////////////////////////////////////////////
// @file main.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <pthread.h>

#include "../MonoGUI/MonoGUI.h"
#include "Images.h"
#include "MyApp.h"
#include "Desktop.h"

#define LCDW   SCREEN_W
#define LCDH   SCREEN_H


// Variables of X Window
static const int xwindow_w = LCDW * SCREEN_MODE;
static const int xwindow_h = LCDH * SCREEN_MODE;
static Display *display;
static int screen_num;
static Window root_win;
static XSetWindowAttributes attribute;
static GC gc;
static Visual* visual;
static Pixmap           g_pixmap;
static int              g_bmppad;
static int              g_bitmap_depth;
static unsigned int     g_bpp;
static unsigned char*   g_imgbuf;

// Varialbes of MonoGUI System
static void lcd_display();
static int fn_event_process();
static LCD              g_LCD;
static MyApp            g_App;
static Desktop          g_Desktop;


int main( int argc, char** argv )
{
	// 检查基本数据类型的大小，如果与预期不符，提醒用户根据所用开发环境做出调整
	DebugPrintf ("-------- Basic data type check: -----------\n");
	DebugPrintf ("sizeof(BYTE)=%ld, should be 1\n", sizeof(BYTE));
	DebugPrintf ("sizeof(WORD)=%ld, should be 2\n", sizeof(WORD));
	DebugPrintf ("sizeof(DWORD)=%ld, should be 4\n", sizeof(DWORD));
	DebugPrintf ("sizeof(ULONGLONG)=%ld, should be 8\n", sizeof(ULONGLONG));
	DebugPrintf ("sizeof(SY_INDEX_ITEM)=%ld, should be 12\n", sizeof(SY_INDEX_ITEM));
	DebugPrintf ("sizeof(HZ_INDEX_ITEM)=%ld, should be 8\n", sizeof(HZ_INDEX_ITEM));
	DebugPrintf ("--------------------------------------------\n");

	// 初始化液晶屏
	g_LCD.XWinInitVirtualLCD (LCDW, LCDH);

	// 应用程序的初始化
	g_App.Create (&g_Desktop, &g_LCD, fn_event_process);

	// 创建MonoGUI的主窗口
	g_Desktop.Create(NULL,WND_STYLE_NORMAL,WND_STATUS_NORMAL,0,0,LCDW,LCDH,0);

	//初始化图片管理器，OImgButton控件会用到图片资源
	InitImageMgt (g_App.m_pImageMgt);

	// 和X服务器连接
	char* display_name = NULL;
	if( (display = XOpenDisplay(display_name)) == NULL )
	{
		printf( "Cannot connect to X server %s \n", XDisplayName(display_name) );
		return (-1);
	}

	// 获得缺省的screen_num
	screen_num = DefaultScreen( display );
	visual = DefaultVisual(display, screen_num);
	g_bitmap_depth = DefaultDepth(display, screen_num);

	attribute.backing_store	= Always;
	attribute.background_pixel	= BlackPixel(display, screen_num);

	// 建立窗口
	printf ("xwindow w=%d, h=%d\n", xwindow_w, xwindow_h);
	root_win = XCreateWindow(display,
				RootWindow(display, screen_num),
				0, 0, xwindow_w, xwindow_h,
				2, g_bitmap_depth,
				InputOutput,
				visual,
				CWBackPixel | CWBackingStore,
				&attribute);

	
	g_bmppad = XBitmapPad(display);

	// 建立GC
	gc = XCreateGC( display, root_win, 0, NULL );

	// 建立位图
	g_pixmap = XCreatePixmap(display, root_win,
				xwindow_w, xwindow_h, g_bitmap_depth);

	g_bpp = (g_bmppad + 7) / 8;
	int imgbuf_size = g_bpp * xwindow_w * xwindow_h;
	g_imgbuf = new unsigned char[imgbuf_size];
	memset (g_imgbuf, 0x0, imgbuf_size);

	// 显示窗口
	XMapWindow(display, root_win);

	// 选择窗口感兴趣的事件
	XSelectInput(display, root_win,
		ExposureMask      |
		KeyPressMask      |
		ButtonPressMask   |
		ButtonReleaseMask |
		Button1MotionMask |
		StructureNotifyMask);

	// 进入事件循环
	while (fn_event_process()) {

	};

	XFreePixmap(display, g_pixmap);
	XCloseDisplay(display);
	delete [] g_imgbuf;
}

void lcd_display()
{
	int x, y;
	for (y = 0; y < xwindow_h; y ++) {
		for (x = 0; x < xwindow_w; x ++) {
			unsigned char* pixel = g_imgbuf + g_bpp * (x + y * xwindow_w);
			int lcd_x = x / SCREEN_MODE;
			int lcd_y = y / SCREEN_MODE;
			BYTE cr = g_LCD.GetPixel (lcd_x, lcd_y) ? 0xFF : 0x0;
			memset (pixel, cr, g_bpp);
		}
	}

	XImage* image = XCreateImage(display, visual, g_bitmap_depth, ZPixmap,
		 0, (char *)g_imgbuf,
		 xwindow_w, xwindow_h, g_bmppad, 0);

	XPutImage(display, g_pixmap, gc, image, 0, 0, 0, 0, xwindow_w, xwindow_h);
	XCopyArea(display, g_pixmap, root_win, gc, 0, 0, xwindow_w, xwindow_h, 0, 0);
	XFree (image);
}

int fn_event_process()
{
	// 取得队列中的事件
	XEvent event;
	XNextEvent (display, &event);
	switch(event.type)
	{
	case Expose:
		// 曝光事件,窗口应重绘
		// 取得最后一个曝光事件(避免重复绘图)
		if( event.xexpose.count != 0 )
		break;

		// 绘制图形
		g_App.PaintWindows(&g_Desktop);
		break;

	case ConfigureNotify:
		XResizeWindow(display, root_win, xwindow_w, xwindow_h);
		break;

	case ButtonPress:
	case ButtonRelease:
		printf("XEvent: MouseKey x=%d, y=%d,", event.xbutton.x, event.xbutton.y);
		if (event.type == ButtonPress) printf (" DOWN\n");
		if (event.type == ButtonRelease) printf (" UP\n");
		if (event.xbutton.button == Button1)
		{
			int nMessageType = 0;
			switch (event.type) {
			case ButtonPress:    nMessageType=OM_LBUTTONDOWN; break;
			case ButtonRelease:  nMessageType=OM_LBUTTONUP;   break;
			}
			// 将系统消息转送给MonoGUI的消息队列
			int mouse_x = event.xbutton.x / SCREEN_MODE;
			int mouse_y = event.xbutton.y / SCREEN_MODE;
			g_App.PostMouseMsg (nMessageType, mouse_x, mouse_y);
		}
		break;

	case MotionNotify:
		printf("XEvent: MouseMove x=%d, y=%d\n", event.xbutton.x, event.xbutton.y);
		if (event.xbutton.button == Button1)
		{
			// 将系统消息转送给MonoGUI的消息队列
			int mouse_x = event.xbutton.x / SCREEN_MODE;
			int mouse_y = event.xbutton.y / SCREEN_MODE;
			g_App.PostMouseMsg(OM_MOUSEMOVE, mouse_x, mouse_y);
		}
		break;

	case KeyPress:
		char buffer[1024];
		KeySym keysym;
		XComposeStatus compose;
		XLookupString( &event.xkey, buffer, 1024, &keysym, &compose );
		printf("XEvent: Key Value is: 0X%lx\n", keysym);
		g_App.PostKeyMsg (keysym);
		break;

	default:
		//g_App.PaintWindows(&g_Desktop);
		break;
	}

	// MonoGUI运行
	if (g_App.RunInEmulator())
	{
		lcd_display();
		// 程序继续运行
		return TRUE;
	}

	// 要求程序退出
	return FALSE;
}
// END
