////////////////////////////////////////////////////////////////////////////////
// @file Desktop.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined (__DESKTOP_H__)
#define __DESKTOP_H__

// Repaint callback
typedef void(*FnShowLCD)();


class Desktop : public OWindow
{
public:
	Desktop();
	virtual ~Desktop();

	// 虚函数，绘制窗口，只绘制附加的滚动条
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

	// 窗口创建后的初始化处理
	virtual void OnInit();

private:
	// 显示功能演示对话框
	void ShowDemoDialog();
};

#endif // !defined(__DESKTOP_H__)
