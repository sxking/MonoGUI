// HSKEY.cpp

#include "stdafx.h"
#include "DTEditor.h"
#include "KeyConfig.h"

// 定义全局函数
// 从字符串中取得一行
BOOL GetLine(char* cString, char* cReturn, int k)
{
	if( cString == NULL )
		return FALSE;
	
	int iStartPos  = 0;
	int iNextPos   = -1;
	int iStrLength = strlen (cString);

	int i;
	for (i=0; i<=k; i++)
	{
		// 把上次查找的位置给开始位置
		iStartPos += (iNextPos + 1);

		// 查找分隔符'\r'
		if (iStartPos < iStrLength)
		{
			iNextPos = strcspn ((cString+iStartPos), "\n");
		}
		else
		{
			return FALSE;
		}
	}
 
	// 将iStartPos和iEndPos之间的内容拷贝给temp
	char* cTemp = new char [iNextPos + 1];
	memset (cTemp, 0x0, iNextPos+1);
	memcpy (cTemp ,(cString+iStartPos), iNextPos);

	// 如果最末字符是“\r”，则去掉
	iStrLength = strlen (cTemp);
	if (cTemp[iStrLength - 1] == '\r')
	{
		iNextPos --;
	}

	strncpy (cReturn, cTemp, iNextPos);
	cReturn[iNextPos] = '\0';

	delete [] cTemp;
	return TRUE;
}

// 从字符串中取得两个“;”之间的部分
BOOL GetSlice(char* cString, char* cReturn, int k)
{
	if(cString == NULL)
		return FALSE;
	int iStartPos  = 0;
	int iNextPos   = -1;
	int iStrLength = strlen (cString);

	int i;
	for (i=0; i<=k; i++)
	{
		// 把上次查找的位置给开始位置（跳过分号）
		iStartPos += (iNextPos + 1);

		// 查找分隔符';'
		if (iStartPos < iStrLength)
		{
			iNextPos = strcspn ((cString+iStartPos), ";");
		}
		else
		{
			return FALSE;
		}
	}
 
	// 将iStartPos和iEndPos之间的内容拷贝给cReturn
	memcpy (cReturn ,(cString+iStartPos), iNextPos);
	return TRUE;
}

// 定义成员函数
CKeyConfig::CKeyConfig()
{
}

CKeyConfig::~CKeyConfig()
{
}

BOOL CKeyConfig::Load(char* psFileName)
{
	// 打开文件
	FILE* fp = fopen(psFileName, "r");
	if (fp == NULL)
		return FALSE;

	// 得到文件长度
    if (fseek(fp, 0, SEEK_END))
	{
		fclose(fp);
		return FALSE;
	}

	unsigned long lFileLen = ftell(fp);

	// 创建一个与文件等长的数组
	char* pcaFile = new char[lFileLen];

	// 将文件内容读入数组
    if (fseek(fp, 0, SEEK_SET))
	{
		fclose(fp);
		return FALSE;
	}

	unsigned long lReadLen = fread (pcaFile, sizeof(char), lFileLen, fp);

	if (lReadLen != lFileLen)
	{
	//	fclose(fp);
	//	return FALSE;
	}

	// 关闭文件
	fclose(fp);

	// 用于存放一行内容的缓冲区
	char psTemp[256];
	char psKeyName[32];
	char psKeyValue[8];

	m_nCount = 0;
	// 逐行解析文件
	int k = 0;
	while( GetLine( pcaFile, psTemp, k ) )
	{
		// 如果开头不是“#define ”则跳过
		if( strncmp( psTemp, "#define", 7 ) != 0 )
		{
			k ++;
			continue;
		}

		memset( psKeyName, 0x0, 32 );
		memset( psKeyValue, 0x0, 8 );

		int t = 7;
		int cur = 0;

		int nMaxLen = strlen( psTemp );

		// 找出宏名称
		while( (psTemp[t] == ' ')  ||
			   (psTemp[t] == '\t') ||
			   (psTemp[t] == '/')  ||
			   (psTemp[t] == '\0') )
		{
			t ++;
			if( t > nMaxLen )
				break;
		}

		if( t > nMaxLen )
		{
			k ++;
			continue;
		}

		while( (psTemp[t] != ' ')  &&
			   (psTemp[t] != '\t') &&
			   (psTemp[t] != '/')  &&
			   (psTemp[t] != '\0') )
		{
			psKeyName[cur] = psTemp[t];
			t ++;
			cur ++;

			if( t > nMaxLen )
				break;
		}

		if( t > nMaxLen )
		{
			k ++;
			continue;
		}

		psKeyName[cur] = '\0';

		// 找出宏值
		cur = 0;

		while( (psTemp[t] == ' ')  ||
			   (psTemp[t] == '\t') ||
			   (psTemp[t] == '/')  ||
			   (psTemp[t] == '\0') )
		{
			t ++;
			if( t > nMaxLen )
				break;
		}

		if( t > nMaxLen )
		{
			k ++;
			continue;
		}

		while( (psTemp[t] != ' ')  &&
			   (psTemp[t] != '\t') &&
			   (psTemp[t] != '/')  &&
			   (psTemp[t] != '\0') )
		{
			psKeyValue[cur] = psTemp[t];
			t ++;
			cur ++;

			if( t > nMaxLen )
				break;
		}

		if( t > nMaxLen )
		{
			k ++;
			continue;
		}

		psKeyValue[cur] = '\0';

		// 如果宏名或者宏指长度为零则进入下一个循环
		if( strlen( psKeyName ) == 0 )
		{
			k ++;
			continue;
		}

		if( strlen( psKeyValue ) == 0 )
		{
			k ++;
			continue;
		}

		int nKeyValue = 0;
		// 将宏值转换为数值
		// 区分十六进制数和十进制数
		if( (strncmp( psKeyValue, "0X", 2 ) == 0) ||
			(strncmp( psKeyValue, "0x", 2 ) == 0) )
		{
			// 十六进制数
			char cc = psKeyValue[2];

			if( cc>='0' && cc<='9' )
			{
				nKeyValue = (cc - '0');
			}
			else if( cc>='a' && cc<='f' )
			{
				nKeyValue = 10 + (cc - 'a');
			}
			else if( cc>='A' && cc<='F' )
			{
				nKeyValue = 10 + (cc - 'A');
			}
			else
			{
				// 存在非法字符
				k ++;
				continue;
			}

			cc =  psKeyValue[3];

			if( cc != '\0' )
			{
				if( cc>='0' && cc<='9' )
				{
					nKeyValue = nKeyValue * 16 + (cc - '0');
				}
				else if( cc>='a' && cc<='f' )
				{
					nKeyValue = nKeyValue * 16 + 10 + (cc - 'a');
				}
				else if( cc>='A' && cc<='F' )
				{
					nKeyValue = nKeyValue * 16 + 10 + (cc - 'A');
				}
				else
				{
					// 存在非法字符
					k ++;
					continue;
				}
			}
		}
		else
		{
			// 十进制数
			nKeyValue = atoi( psKeyValue );
			if( nKeyValue == 0 )
			{
				k ++;
				continue;
			}
		}

		k ++;

		// 将数值添加到KeyMap数组中
		memset( m_pstKeyMap[ m_nCount ].psKeyName, 0x0, 32 );
		strncpy( m_pstKeyMap[ m_nCount ].psKeyName, psKeyName, 32 );
		m_pstKeyMap[ m_nCount ].nKeyValue = nKeyValue;
		m_nCount ++;

		if( m_nCount == KEY_MAX )
			break;
	}

	delete [] pcaFile;

	return TRUE;
}

int CKeyConfig::Find(char* psKeyName)
{
	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (strncmp(psKeyName, m_pstKeyMap[i].psKeyName, 32) == 0)
		{
			return i;
		}
	}

	return -1;
}

int CKeyConfig::GetCount()
{
	return m_nCount;
}

BOOL CKeyConfig::Get(int nIndex, char* psKeyName)
{
	if ((nIndex < 0) || (nIndex >= m_nCount))
		return FALSE;

	int nLen = strlen(m_pstKeyMap[ nIndex ].psKeyName);
	strncpy(psKeyName, m_pstKeyMap[ nIndex ].psKeyName, 32);
	psKeyName[nLen] = '\0';

	return TRUE;
}

/* END */