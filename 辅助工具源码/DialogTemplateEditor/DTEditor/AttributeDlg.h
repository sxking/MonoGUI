#if !defined(AFX_ATTRIBUTEDLG_H__1A945F32_05DC_4FE0_8EF6_B25A96F6A202__INCLUDED_)
#define AFX_ATTRIBUTEDLG_H__1A945F32_05DC_4FE0_8EF6_B25A96F6A202__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AttributeDlg.h : header file
//

class CEditorView;
class CCtrlMgt;
/////////////////////////////////////////////////////////////////////////////
// CAttributeDlg dialog

class CAttributeDlg : public CDialog
{
public:
	CEditorView* m_pView;
	BOOL m_bEditEnableState;
	int m_iSelectedIndex;

// Construction
public:
	CAttributeDlg(CEditorView* pParent);   // standard constructor
	BOOL Create();

	void SetText(CString strName, CString strValue);
	void Renew(BOOL bEffect);
	void DiaplayAttribute();

// Dialog Data
	//{{AFX_DATA(CAttributeDlg)
	enum { IDD = IDD_ATTRIBUTE };
	CButton	m_btnEditStyle;
	CEdit	m_editValue;
	CListCtrl	m_lstAttribute;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAttributeDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAttributeDlg)
	virtual void OnCancel();
	afx_msg void OnClickList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusEdit();
	afx_msg void OnEditStyle();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ATTRIBUTEDLG_H__1A945F32_05DC_4FE0_8EF6_B25A96F6A202__INCLUDED_)
